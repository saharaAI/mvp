import streamlit as st
from services.llm_service import AITaskOrchestrator
from app.utils import hide_streamlit_elements





def main():
    st.set_page_config(page_title="Agent App", layout="wide")
    hide_streamlit_elements()

    orchestrator = AITaskOrchestrator()
    orchestrator.main()



if __name__ == "__main__":
    main()