import os
import logging
import streamlit as st
from services.data_processor import GenAIHandler, FileProcessor, PromptGenerator, JsonUtils
def main():
    from config import KEYS, MODEL_NAME, UPLOAD_DIR
    from app.utils import hide_streamlit_elements
    hide_streamlit_elements()

    # Configure logging
    logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')
    logger = logging.getLogger(__name__)

    # Initialize AI Handler
    ai_handler = GenAIHandler(keys=KEYS, model_name=MODEL_NAME)
    logger.info("Initialized GenAIHandler")

    # Initialize Prompt Generator
    prompt_generator = PromptGenerator(ai_handler=ai_handler)
    logger.info("Initialized PromptGenerator")


    logger.info("Starting Streamlit application")
    st.markdown("# Téléchargement de Fichiers et Prompts 📁")

    uploaded_file = st.file_uploader("Choose a file", type=['csv', 'xlsx', 'xls', 'pdf'])

    if uploaded_file is not None:
        file_path = os.path.join(UPLOAD_DIR, uploaded_file.name)
        os.makedirs(UPLOAD_DIR, exist_ok=True)  # Ensure the upload directory exists
        with open(file_path, "wb") as f:
            f.write(uploaded_file.getbuffer())
        logger.info(f"Uploaded file saved at {file_path}")

        if st.button("Analyser"):
            logger.info("Analyse button clicked")
            with st.spinner("Analyse en cours..."):
                try:
                    file_content = FileProcessor.process_uploaded_file(file_path)
                    logger.info("File processed successfully")

                    prompts = prompt_generator.generate_prompts(file_content)
                    logger.info("Prompts generated successfully")

                    user_prompt = prompts[0]["prompt"]
                    prompt_description = prompts[0]["description"]

                    st.write(f"**Prompt:** {user_prompt}")

                    if prompt_description:
                        st.write(f"**Description:** {prompt_description}")

                    result = ai_handler.get_response(user_prompt, "", file_content, [])
                    st.write(f"**Résultat:** {result}")
                    logger.info("AI response received successfully")

                except Exception as e:
                    logger.error(f"Error during analysis: {e}")
                    st.error(f"An error occurred: {e}")
            
if __name__ == "__main__":
    main()
