import hashlib
import os
import random
from typing import List, Dict
import io
import streamlit as st
import google.generativeai as genai
from unstructured.partition.auto import partition
import markdown
import pyperclip
import pandas as pd
import json
import re



class GenAIHandler:
    def __init__(self, keys: List[str], model_name: str = "gemini-1.5-pro"):
        self.keys = keys
        self.model_name = model_name
        genai.configure(api_key=random.choice(self.keys))
        
    def get_response(self, user_input: str, prompt: str, context: str, history: List[Dict[str, str]]) -> str:
        generation_config = {
            "temperature": 0.2,  # Lowered temperature for more focused outputs
            "top_p": 0.95,
            "top_k": 40,
            "max_output_tokens": 2048,
        }
        model = genai.GenerativeModel(model_name=self.model_name, generation_config=generation_config)
        formatted_history = [
            {"role": "user" if msg["role"] == "user" else "model", "parts": [msg.get("parts", [msg.get("content", "")])[0]]}
            for msg in history
        ]
        chat_session = model.start_chat(history=formatted_history)
        full_prompt = f"{prompt}\n\nDocument content:\n{context}\n\nUser question: {user_input}\n\nAnswer:"
        
        return chat_session.send_message(full_prompt).text


class FileProcessor:
    @staticmethod
    @st.cache_data
    def process_uploaded_file(file_path: str) -> str:
        _, file_extension = os.path.splitext(file_path)
        
        if file_extension.lower() in ['.csv', '.xlsx', '.xls']:
            if file_extension.lower() == '.csv':
                df = pd.read_csv(file_path)
            else:
                df = pd.read_excel(file_path)
            return df.to_string()
        elif file_extension.lower() == '.pdf':
            elements = partition(filename=file_path)
            return "\n\n".join(str(el) for el in elements)
        else:
            return "Unsupported file type"


class JsonUtils:
    @staticmethod
    def clean_json_string(json_string: str) -> List[Dict[str, str]]:
        # Remove any leading/trailing whitespace and newlines
        json_string = json_string.strip()
        
        # Remove any text before the first '[' and after the last ']'
        json_match = re.search(r'\[.*\]', json_string, re.DOTALL)
        if json_match:
            json_string = json_match.group()
        else:
            return []
        
        # Replace any single quotes with double quotes
        json_string = json_string.replace("'", '"')
        
        try:
            # Parse the entire string as a JSON array
            return json.loads(json_string)
        except json.JSONDecodeError:
            # If parsing fails, try to salvage individual items
            items = re.findall(r'\{.*?\}', json_string)
            result = []
            for item in items:
                try:
                    result.append(json.loads(item))
                except json.JSONDecodeError:
                    continue
            return result


class PromptGenerator:
    def __init__(self, ai_handler: GenAIHandler):
        self.ai_handler = ai_handler
        
    def generate_prompts(self, file_content: str) -> List[Dict[str, str]]:
        prompt = f"""
        ALWAYS ANSWER WITH FRENCH ONLY!! !
        Based on the following file content, generate minimum 5 relevant prompts that a user might want to ask about the data.
        Format the prompts as a JSON list of dictionaries, where each dictionary has a 'prompt' key and a 'description' key.

        File content:
        {file_content[:5000]}  # Limit to first 1000 characters to avoid overwhelming the model

        THE OUTPUT MUST BE A VALID JSON LIST OF DICTIONARIES. EXAMPLE FORMAT:
        [
            {{"prompt": "Summarize the main points", "description": "Get an overview of the key information"}},
            {{"prompt": "Analyze trends in the data", "description": "Identify patterns and trends in the dataset"}},
            {{"prompt": "Find relationships between variables", "description": "Identify relationships between variables in the dataset"}},
            {{"prompt": "Identify outliers", "description": "Find outliers in the dataset"}},
            {{"prompt": "Make predictions", "description": "Make predictions about the dataset"}}
        ]
        """
        
        response = self.ai_handler.get_response("", prompt, "", [])
        cleaned_response = JsonUtils.clean_json_string(response)
        return cleaned_response


