import subprocess
import os
import shutil

example_prompt = """output shoukd have this format, markdown file as : 
# Title Slide {.title-slide}
## Subtitle

***

## Two Column Layout {.two-column}

<div class="column">

- Item 1
- Item 2
- Item 3

</div>

<div class="column">

Some text in the second column.

</div>

***

## Custom List

<ul class="custom-list">
  <li>First item</li>
  <li>Second item</li>
  <li>Third item</li>
</ul>

***

> This is a quote slide {.quote-slide}

"""



def markdown_to_presentation(input_file, output_dir):
    """
    Convert markdown to HTML presentation using markdown-to-presentation CLI.
    This function creates a temporary directory for the build process.
    
    :param input_file: Path to the input markdown file
    :param output_dir: Directory to output the HTML and assets
    :return: Path to the generated index.htm file
    """
    # Create a temporary directory for the build process
    temp_dir = "temp_build_dir"
    os.makedirs(temp_dir, exist_ok=True)

    try:
        # Copy the input markdown file to the temp directory
        shutil.copy(input_file, os.path.join(temp_dir, "slides.md"))

        # Create assets directory in the temp directory
        assets_dir = os.path.join(temp_dir, "assets")
        os.makedirs(assets_dir, exist_ok=True)

        # Create theme CSS file
        with open(os.path.join(assets_dir, "_theme.scss"), "w") as f:
            f.write("""
// Override default reveal.js theme variables here

$backgroundColor: #f0f0f0;
$mainColor: #333;
$headingColor: #2c3e50;

$mainFontSize: 32px;
$mainFont: 'Source Sans Pro', Helvetica, sans-serif;
$headingFont: 'Playfair Display', serif;
$headingTextShadow: none;
$headingLetterSpacing: normal;
$headingTextTransform: none;
$headingFontWeight: 600;
$linkColor: #3498db;
$linkColorHover: lighten($linkColor, 20%);

$slideNumberColor: #888;
$progressBarColor: #3498db;

.reveal {
  section img {
    border: 4px solid $mainColor;
    box-shadow: 0 0 10px rgba(0, 0, 0, 0.15);
  }
  
  .controls {
    color: $linkColor;
  }
}
""")

        # Create app CSS file
        with open(os.path.join(assets_dir, "_app.scss"), "w") as f:
            f.write("""
// Custom styles for your specific presentation

.highlight {
  color: #e74c3c;
  font-weight: bold;
}

.big-text {
  font-size: 1.5em;
}

.small-text {
  font-size: 0.8em;
}

.two-column {
  display: flex;
  justify-content: space-between;
  
  .column {
    width: 48%;
  }
}

.title-slide {
  h1 {
    font-size: 2.5em;
    margin-bottom: 0.5em;
  }
  
  h2 {
    font-size: 1.5em;
    color: #7f8c8d;
  }
}

.quote-slide {
  background-color: #34495e;
  color: #ecf0f1;
  
  blockquote {
    border-left: 5px solid #3498db;
    padding-left: 20px;
    font-style: italic;
  }
}

.custom-list {
  list-style-type: none;
  padding-left: 0;
  
  li {
    margin-bottom: 0.5em;
    padding-left: 1.5em;
    position: relative;
    
    &:before {
      content: '→';
      position: absolute;
      left: 0;
      color: #3498db;
    }
  }
}

@media screen and (max-width: 768px) {
  .two-column {
    flex-direction: column;
    
    .column {
      width: 100%;
      margin-bottom: 1em;
    }
  }
}""")

        # Change to the temporary directory
        original_dir = os.getcwd()
        os.chdir(temp_dir)

        # Prepare the command
        cmd = ["markdown-to-presentation", "run-build"]

        # Run the command
        try:
            result = subprocess.run(cmd, check=True, capture_output=True, text=True)
            print(result.stdout)
        except subprocess.CalledProcessError as e:
            print(f"Error occurred: {e}")
            print(f"stdout: {e.stdout}")
            print(f"stderr: {e.stderr}")
            raise
        finally:
            # Change back to the original directory
            os.chdir(original_dir)

        # Copy the generated files to the output directory
        shutil.copytree(temp_dir, output_dir, dirs_exist_ok=True)

    finally:
        # Clean up the temporary directory
        shutil.rmtree(temp_dir, ignore_errors=True)

    # Return the path to the generated index.htm file
    return os.path.join(output_dir, "index.htm")


# Example usage
if __name__ == "__main__":
    input_md = "./response.md"
    output_folder  = "presentation_output"

    try:
        output_file = markdown_to_presentation(input_md, output_folder)
        print(f"Presentation generated successfully. Output file: {output_file}")
    except Exception as e:
        print(f"An error occurred: {str(e)}")# Example usage
